from face_clustering.utils import Error
from io import BytesIO
from PIL import Image
import numpy as np
from sklearn.cluster import DBSCAN
import face_recognition
import base64
import cv2

def decode_image(byte_string):
    try:
        img = Image.open(BytesIO(base64.b64decode(byte_string)))
        img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
        rgb = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)
        # img.save('img.jpeg')
        return rgb
    except Exception as ex:
        print(ex)
        raise Error('Could not read image')


def extract_features(image):
    print("[INFO] extracting features...")
    boxes = face_recognition.face_locations(image, model="cnn")
    face_features = face_recognition.face_encodings(image, boxes)
    return face_features


def make_clusters(face_features_tuple):
    # Extract face features and paths
    face_features = [feature for path, feature in face_features_tuple]
    paths = [path for path, feature in face_features_tuple]
    #print(face_features)
    result = {}
    encodings = []
    for i in face_features:
        encodings.append(i[0])
    # cluster the embeddings
    print("[INFO] clustering...")
    clt = DBSCAN(eps=0.6, min_samples=1, metric="euclidean")
    clt.fit(encodings)
    #clt.fit(face_features)

    # determine the total number of unique faces found in the dataset
    labelIDs = np.unique(clt.labels_)
    numUniqueFaces = len(labelIDs)
    print("[INFO] # unique faces: {}".format(numUniqueFaces))

    # loop over the unique face integers
    for labelID in labelIDs:
        # find all indexes into the `data` array that belong to the
        # current label ID
        print("[INFO] faces for face ID: {}".format(labelID))
        idxs = np.where(clt.labels_ == labelID)[0]
   
        # initialize the list of faces to include in the montage
        paths_to_faces = []
        # loop over the sampled indexes
        for i in idxs:
            paths_to_faces.append(paths[i])
        print("pathes for face#", labelID, paths_to_faces)
        result[str(labelID)] = paths_to_faces

    return result

with open("D:\\projects\\diploma\\johnny.jpg", "rb") as imageFile:
    str = base64.b64encode(imageFile.read())

with open("D:\\projects\\diploma\\morgan.jpg", "rb") as imageFile:
    str1 = base64.b64encode(imageFile.read())

json_ = {"a" : str,
        "b" : str1}
face_detection(json_)

