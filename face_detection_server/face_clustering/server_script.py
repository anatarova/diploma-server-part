from flask import jsonify, request
from face_clustering import utils
from face_clustering import app
from face_clustering import image_processing


@app.route('/', methods=["GET"])
def index():
    return "Server is running"


@app.route('/face_clustering', methods=["POST"])
def face_detection():
    utils.log("start face clustering")
    try:
        utils.log('detecting faces')
        payload = utils.try_get_json(request)
        # list of tuples which contains (path, face feature)
        features = []
        for path, picture in payload.items():
            image = image_processing.decode_image(picture)
            if (image is None):
                raise utils.Error("image is None")
            face_feature = image_processing.extract_features(image)
            features.append((path, face_feature))
        utils.log('starting make clusters')
        result = image_processing.make_clusters(features)
        return jsonify(result)
    except Exception as ex:
        print(ex)
        raise utils.Error("error while clustering")


@app.errorhandler(utils.Error)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    utils.log(error.message, 'ERROR')
    return response
